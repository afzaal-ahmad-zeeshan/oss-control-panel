# Control Panel for Alibaba Cloud OSS

Alibaba Cloud OSS control panel developed using Spring Boot and Java.

![Alibaba Cloud OSS client homepage image](homepage.png)

## Features

Application showcases how to:

1. Connect to OSS service.
1. Query the bucket details.
1. Query the objects in buckets.

In future, I might add more features for the OSS control panel.

### `application.properties`

Configure a `application.properties` file in your resources folder with the
following details:

``` text
spring.cloud.alicloud.access-key=<your-key>
spring.cloud.alicloud.secret-key=<your-secret>
spring.cloud.alicloud.oss.endpoint=<your-endpoint-from-OSS>
```

## Contributions

You can add your contributions to the project by submitting pull requests to
the repository.
