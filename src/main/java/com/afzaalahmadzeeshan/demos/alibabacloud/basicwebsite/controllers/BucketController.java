package com.afzaalahmadzeeshan.demos.alibabacloud.basicwebsite.controllers;

import java.io.File;

import com.aliyun.oss.OSS;
import com.aliyun.oss.model.BucketInfo;
import com.aliyun.oss.model.UploadFileRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

@Controller("/bucket")
public class BucketController {
    @Autowired OSS ossClient;

    @GetMapping("/bucket/{bucketName}")
    public String getBucketDetails(@PathVariable String bucketName, Model viewModel) {
        System.out.println(String.format("[INFO] {bucketName} got request for %s.", bucketName));
        if(ossClient.doesBucketExist(bucketName)) {
            BucketInfo bucketInfo = ossClient.getBucketInfo(bucketName);
            viewModel.addAttribute("bucket", bucketInfo);
        }
        return "/buckets-index-get";
    }

    @GetMapping("/bucket/{bucketName}/objects")
    public String getBucketObjects(@PathVariable String bucketName) {
        System.out.println(String.format("[INFO] {bucketName}/objects got request for %s.", bucketName));
        return "/buckets-objects-get";
    }

    @PostMapping("/bucket/{bucketName}/upload")
    public String uploadToBucket(@PathVariable String bucketName, File file) {
        return "Uploaded";
    }
}
