package com.afzaalahmadzeeshan.demos.alibabacloud.basicwebsite.controllers;

import java.util.ArrayList;
import java.util.List;

import com.afzaalahmadzeeshan.demos.alibabacloud.basicwebsite.models.OssBucket;
import com.aliyun.oss.OSS;
import com.aliyun.oss.model.Bucket;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller("/")
public class HomeController {
    @Autowired OSS ossClient;

    @GetMapping("")
    public String getHomePage(Model viewModel) {
        List<Bucket> buckets = ossClient.listBuckets();
        List<OssBucket> modelBuckets = new ArrayList<>();

        for (Bucket b : buckets) {
            OssBucket bucket = new OssBucket();

            bucket.name = b.getName();
            bucket.location = b.getLocation();
            bucket.creationDate = b.getCreationDate().toString();
            bucket.endpoint = b.getExtranetEndpoint();

            modelBuckets.add(bucket);
        }
        viewModel.addAttribute("buckets", modelBuckets);
        return "/home-index-get";
    }

    @GetMapping("about")
    public String getAboutPage() {
        return "/home-about-get";
    }

    @GetMapping("contact")
    public String getContactPage() {
        return "/home-contact-get";
    }
}
