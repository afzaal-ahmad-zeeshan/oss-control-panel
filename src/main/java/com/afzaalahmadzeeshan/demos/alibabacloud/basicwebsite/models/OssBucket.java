package com.afzaalahmadzeeshan.demos.alibabacloud.basicwebsite.models;

public class OssBucket {
    public String name;
    public String location;
    public String creationDate;
    public String endpoint;
}
